# -*- coding: utf-8 -*-
from scrapy import Spider
from scrapy.http import Request

from urlparse import urljoin

from lazada.items import LazadaItem


class WomenHijabsSpider(Spider):
    name = "women_hijabs"
    allowed_domains = ["lazada.com.my"]
    start_urls = (
        'http://www.lazada.com.my/shop-women-hijabs',
    )

    def parse(self, response):

        selector = '//div[contains(@class, "product-card ")]/a'
        for product_card in response.xpath(selector):

            product = LazadaItem()

            url = product_card.xpath("@href").extract()[0]

            product['buy_url'] = url
            product['original_url'] = url

            request = Request(urljoin(response.url, url),
                              callback=self.parse_product)

            request.meta['product'] = product
            yield request

    def parse_product(self, response):

        product = response.meta['product']

        title_selector = '//meta[@property="og:title"]/@content'
        product['name'] = response.xpath(title_selector).extract()[0]

        image_url_selector = '//meta[@property="og:image"]/@content'
        image_url = response.xpath(image_url_selector).extract()[0]

        product['file_urls'] = [image_url]

        file_name = image_url.split('/')[-1]
        product['file_name'] = file_name

        brand_selector = '//div[@itemprop="brand"]/a/span[@itemprop="name"]/text()'
        product['brand'] = response.xpath(brand_selector).extract()[0]

        price_selector = '//span[@id="product_price"]/text()'
        product['price'] = response.xpath(price_selector).extract()[0]

        yield product
