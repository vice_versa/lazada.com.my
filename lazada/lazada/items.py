# -*- coding: utf-8 -*-

import scrapy


class LazadaItem(scrapy.Item):

    file_name = scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()
    brand = scrapy.Field()
    buy_url = scrapy.Field()
    original_url = scrapy.Field()
    gender = scrapy.Field()
    do_not_use_in_algo = scrapy.Field()
    garment_element = scrapy.Field()
    file_urls = scrapy.Field()
    files = scrapy.Field()
