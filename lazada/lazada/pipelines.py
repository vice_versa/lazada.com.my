# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.pipelines.images import FilesPipeline

from .settings import db
from .models import LazadaItem


class LazadaPipeline(object):

    def process_item(self, item, spider):
        record = LazadaItem(file_name=item['file_name'],
                            name=item['name'],
                            price=item['price'],
                            brand=item['brand'],
                            buy_url=item['buy_url'],
                            original_url=item['original_url'])
        db.add(record)
        db.commit()
        return item


class LazadaFilesPipeline(FilesPipeline):

    def file_path(self, request, response=None, info=None):
        path = super(LazadaFilesPipeline, self).file_path(
            request, response, info)
        path = path.split('/')
        path = '/'.join(path[1:])
        return path

    def item_completed(self, results, item, info):
        item = super(LazadaFilesPipeline, self).item_completed(
            results, item, info)
        item['file_name'] = item['files'][0]['path']
        return item
