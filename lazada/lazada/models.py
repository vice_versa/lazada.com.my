# -*- coding: utf-8 -*-

from sqlalchemy import Column, String, Integer, Float, Text
from .settings import Base


class LazadaItem(Base):
    __tablename__ = 'clothes'

    id = Column(Integer, primary_key=True)
    file_name = Column(String(250))
    name = Column(String(128))
    price = Column(Float)
    brand = Column(String(128))
    buy_url = Column(Text)
    original_url = Column(Text)
    gender = Column(Integer)
    slot = Column(String(45))
    do_not_use_in_algo = Column(Integer)
    garment_element = Column(String(45))

    def __init__(self,
                 id=None,
                 file_name=None,
                 name=None,
                 price=None,
                 brand=None,
                 buy_url=None,
                 original_url=None,
                 gender=1,
                 slot=u"slot1",
                 do_not_use_in_algo=1,
                 garment_element=u"accessories"):

        self.id = id

        self.file_name = file_name
        self.name = name
        self.price = price
        self.brand = brand
        self.buy_url = buy_url
        self.original_url = original_url
        self.gender = gender
        self.slot = slot
        self.do_not_use_in_algo = do_not_use_in_algo
        self.garment_element = garment_element
